import React from 'react'
import AddOption from './AddOption'
import Options from './Options'
import Header from './Header'
import Action from './Action'

class IndecisionApp extends React.Component {
  constructor(props) {
    super();
    this.state = {
      options: props.options
    }
    this.handleDeleteAll = this.handleDeleteAll.bind(this)
    this.handlePick = this.handlePick.bind(this)
    this.handleAddOption = this.handleAddOption.bind(this)
    this.handleRemoveOption = this.handleRemoveOption.bind(this)
  }

  componentDidMount() {
    try {
      const json = localStorage.getItem('options')
      const options = JSON.parse(json)
      if (options) {
        this.setState({
          options
        })
      }
      console.log('componentDidMount');
    } catch (error) {

    }

  }

  componentWillUnmount() {
    console.log('componentWillUnmount');
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.options.length !== this.state.options.length) {
      const json = JSON.stringify(this.state.options)
      localStorage.setItem('options', json)
    }
    console.log('componentDidUpdate');
  }

  handleDeleteAll() {
    this.setState({
      options: []
    })
  }

  handleRemoveOption(optionToRemove) {
    this.setState({
      options: this.state.options.filter((option) => option !== optionToRemove)
    })
  }

  handlePick() {
    const randNum = Math.floor(Math.random() * this.state.options.length)
    const option = this.state.options[randNum]
    alert(option)
  }

  handleAddOption(option) {
    if (!option) {
      return 'Enter a Proper Value!'
    } else if (this.state.options.indexOf(option) > -1) {
      return 'Item already exists!'
    }

    this.setState({
      options: this.state.options.concat(option)
    })
  }

  render() {
    const subtitle = "Put your life in the hands of a computer."

    return (
      <div>
        <Header subtitle={subtitle} />
        <Action handlePick={this.handlePick} hasOptions={this.state.options.length > 0} />
        <Options
          handleDeleteAll={this.handleDeleteAll}
          options={this.state.options}
          handleRemoveOption={this.handleRemoveOption} />
        <AddOption handleAddOption={this.handleAddOption} />
      </div>
    )
  }
}

IndecisionApp.defaultProps = {
  options: []
}

export default IndecisionApp
