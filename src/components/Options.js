import React from 'react'
import Option from './Option'

const Options = (props) => {
  return (
    <div>
    {props.options.length === 0 && <p>Please add an Option to get started!</p>}
      {props.options.map((option) => (
        <Option
          handleRemoveOption={props.handleRemoveOption}
          key={option}
          optionText={option} />
        )
      )}
      <button onClick={props.handleDeleteAll}>Remove All</button>
    </div>
  )
}

export default Options
